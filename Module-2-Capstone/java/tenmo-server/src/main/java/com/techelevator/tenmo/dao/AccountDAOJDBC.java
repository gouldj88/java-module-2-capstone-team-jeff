package com.techelevator.tenmo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import com.techelevator.tenmo.model.Account;

@Component
public class AccountDAOJDBC implements AccountsDAO {

	private JdbcTemplate jdbcTemplate;
	
	public AccountDAOJDBC (JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		
	}
	
	public List<Account> listAll() { 												// Lists all Accounts
		List<Account> findAllAccounts = new ArrayList<Account>();
		
		String findAccountsList = "SELECT account_id, user_id, balance FROM accounts";
		
		SqlRowSet list = jdbcTemplate.queryForRowSet(findAccountsList);
		
		while(list.next()) {
			Account account = new Account();
			account = MapRowToAccount(list);
			findAllAccounts.add(account);
		}
		
	return findAllAccounts;
	}
	
	public Account getAccountBalanceByUserId(long user_id) {	
		String sqlGetAccountBalance = "SELECT * FROM accounts WHERE user_id = ?";		
		SqlRowSet accountBalance = jdbcTemplate.queryForRowSet(sqlGetAccountBalance, user_id);
		Account anAccount = new Account();;
		if(accountBalance.next()) {
			anAccount = MapRowToAccount(accountBalance);

		}		
		return anAccount;
	}
	
	public void updateAccount(Account accounts) {
		String sqlUpdateAccount = "UPDATE accounts "
								+ "SET balance = ? WHERE account_id = ?";
		jdbcTemplate.update(sqlUpdateAccount, accounts.getBalance(), accounts.getAccount_id());

	}
	
	public Account MapRowToAccount(SqlRowSet results) {
		
		Account anAccount = new Account();
		anAccount.setAccount_id(results.getLong("account_id"));
		anAccount.setUser_id(results.getLong("user_id"));
		anAccount.setBalance(results.getDouble("balance"));
		
		return anAccount;
	}
	
}
