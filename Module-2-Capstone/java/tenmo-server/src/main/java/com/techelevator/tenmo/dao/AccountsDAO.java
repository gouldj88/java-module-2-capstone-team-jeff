package com.techelevator.tenmo.dao;

import java.util.List;

import com.techelevator.tenmo.model.Account;

public interface AccountsDAO {
	
	
	List<Account> listAll();
	
	Account getAccountBalanceByUserId(long user_id);
	
	void updateAccount(Account accounts);

}
