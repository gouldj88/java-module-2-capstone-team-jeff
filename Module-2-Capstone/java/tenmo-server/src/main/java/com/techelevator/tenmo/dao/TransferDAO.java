package com.techelevator.tenmo.dao;

import java.util.List;

import com.techelevator.tenmo.model.Transfers;
import com.techelevator.tenmo.model.User;

public interface TransferDAO {
	
	
	Transfers createTransfer(Transfers transfer);
	
	Transfers getTransferByTransferId (long transferId);
	
	List<Transfers> getTransferByAccountId(long accountId);
	
}
