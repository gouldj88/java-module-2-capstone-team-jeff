package com.techelevator.tenmo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import com.techelevator.tenmo.model.Transfers;
import com.techelevator.tenmo.model.User;

@Component
public class TransferDAOJDBC implements TransferDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	
	public TransferDAOJDBC (JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
		
	public Transfers createTransfer(Transfers transfer)  {

		

		String createTransfer = "INSERT INTO Transfers(transfer_type_id, transfer_status_id, account_from, account_to, amount) " +
								"VALUES(2, 2, ?, ?, ?)";

		transfer.setTransfer_id(GetNextTransferID());
		
		jdbcTemplate.update(createTransfer, 
											transfer.getAccount_from(), 
											transfer.getAccount_to(), 
											transfer.getAmount());
		
		Transfers aNewTransfer = transfer;
		
		return aNewTransfer;
		
	}
	
	public List<Transfers> getTransferByAccountId(long accountId) {
		List<Transfers> listOfTransfers = new ArrayList<Transfers>();
		
		String sqlTransferByID = "SELECT * FROM transfers WHERE transfers.account_from = ? OR transfers.account_to = ?";
		
		SqlRowSet retrievedTransfer = jdbcTemplate.queryForRowSet(sqlTransferByID, accountId, accountId);
		
		while(retrievedTransfer.next()) {
			listOfTransfers.add(MapRowToTransfers(retrievedTransfer));
			
		}
		
		return listOfTransfers;

	}
	
	public Transfers getTransferByTransferId (long transferId) {
		
		Transfers aTransfer = null;
		
		String sqlTransferByID = "SELECT * FROM transfers WHERE transfer_id = ?";
		
		SqlRowSet retrievedTransfer = jdbcTemplate.queryForRowSet(sqlTransferByID, transferId);
		
		if(retrievedTransfer.next()) {
			aTransfer = MapRowToTransfers(retrievedTransfer);
		}
		return aTransfer;
	}
	
	
	public long GetNextTransferID() {
		
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_transfer_id')");
		
		if(nextIdResult.next()) {               // if the SQLRowSet called nextIDResult has any data, move to the next line of data
			return nextIdResult.getLong(1);     // get the long value in column 1 of the line and return it
		} else {                                // if there is no data in nextIDResult, throw a RuntimeException
			throw new RuntimeException("Something went wrong while getting an id for the transfer");
		}
	}
	
	public Transfers MapRowToTransfers(SqlRowSet results) {
		
		Transfers aTransfer = new Transfers();
		aTransfer.setTransfer_id(results.getLong("transfer_id"));
		aTransfer.setTransfer_type_id(results.getLong("transfer_type_id"));
		aTransfer.setTransfer_status_id(results.getLong("transfer_status_id"));
		aTransfer.setAccount_from(results.getInt("account_from"));
		aTransfer.setAccount_to(results.getInt("account_to"));
		aTransfer.setAmount(results.getDouble("amount"));
		
		return aTransfer;
	}
	
}
