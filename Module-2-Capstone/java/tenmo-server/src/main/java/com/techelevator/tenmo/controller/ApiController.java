package com.techelevator.tenmo.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techelevator.tenmo.dao.AccountDAOJDBC;
import com.techelevator.tenmo.dao.AccountsDAO;
import com.techelevator.tenmo.dao.TransferDAO;
import com.techelevator.tenmo.dao.UserDAO;
import com.techelevator.tenmo.model.Account;
import com.techelevator.tenmo.model.Transfers;




/*******************************************************************************************************
 * This is where you code any API controllers you may create
********************************************************************************************************/
@PreAuthorize("isAuthenticated()")
@RestController
public class ApiController {

	private TransferDAO transferDAO;
	private AccountsDAO accountDAO;
	private UserDAO userDAO;
	
    public ApiController(AccountDAOJDBC accountDAO, UserDAO userDAO, TransferDAO transferDAO) {
        this.accountDAO = accountDAO;
        this.userDAO = userDAO;
        this.transferDAO = transferDAO;
     
    }
	
    @RequestMapping(path = "/accounts/{id}/balance", method = RequestMethod.GET)
    public Account getAccountBalance(@PathVariable int id) {
        return accountDAO.getAccountBalanceByUserId(id); // Good
}
    
    
    @RequestMapping(path = "/accounts", method = RequestMethod.GET)
    public List<Account> getAccountList() {
    	
    	return accountDAO.listAll();
    }
    
//    @RequestMapping(path = "/accounts/{id}", method = RequestMethod.PUT)
//    public Account update (@RequestBody Account account, @PathVariable int id) {
//    	
//    	return accountDAO.updateAccount(account); // No idea
//    }
    
    @RequestMapping(path = "/transfers/{id}", method = RequestMethod.GET)
    public Transfers getTransferByTransferId(@PathVariable long id) {
    	return transferDAO.getTransferByTransferId(id);
    	
    }
    
    @RequestMapping(path = "/accounts/{id}/transfers", method = RequestMethod.GET)
    public List<Transfers> getTransferByAccountId(@PathVariable int id) {

        return transferDAO.getTransferByAccountId(id);
    }
    
    @RequestMapping(path = "/accounts/transfers", method = RequestMethod.POST)
    public Transfers createTransfer (@RequestBody Transfers transfer) {
    	Account account1 = accountDAO.getAccountBalanceByUserId(transfer.getAccount_to());
    	Account account2 = accountDAO.getAccountBalanceByUserId(transfer.getAccount_from());
    	account1.setBalance(account1.getBalance() + transfer.getAmount()) ;
    	account2.setBalance(account2.getBalance() - transfer.getAmount());
    	accountDAO.updateAccount(account1);
    	accountDAO.updateAccount(account2);
    	return transferDAO.createTransfer(transfer);
}
}