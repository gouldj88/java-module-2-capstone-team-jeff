package com.techelevator.tenmo.models;

public class Transfers {
	
private long transfer_id;
private long transfer_type_id;
private long transfer_status_id;
private int account_from;
private int account_to;
private double amount;

public long getTransfer_id() {
	return transfer_id;
}
public void setTransfer_id(long transfer_id) {
	this.transfer_id = transfer_id;
}
public long getTransfer_type_id() {
	return transfer_type_id;
}
public void setTransfer_type_id(long transfer_type_id) {
	this.transfer_type_id = transfer_type_id;
}
public long getTransfer_status_id() {
	return transfer_status_id;
}
public void setTransfer_status_id(long transfer_status_id) {
	this.transfer_status_id = transfer_status_id;
}
public int getAccount_from() {
	return account_from;
}
public void setAccount_from(int account_from) {
	this.account_from = account_from;
}
public int getAccount_to() {
	return account_to;
}
public void setAccount_to(int account_to) {
	this.account_to = account_to;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}

@Override
public String toString() {
	return "-----------------------------------------------\n"
		 + "Transfers"
		 + "\nTransfer ID: " + transfer_id
		 + "\nTransfer Type: " + transfer_type_id 
		 + "\nTransfer Status: " + transfer_status_id
		 + "\nAccount From: " + account_from 
		 + "\nAccount To: " + account_to
		 + "\nAmmount: " + amount
		 + "\n-----------------------------------------------";
}


}
