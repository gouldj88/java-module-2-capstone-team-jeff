package com.techelevator.tenmo.models;

public class Account {
	private long account_id;
	private long user_id;
	private double balance;
	/**
	 * @return the account_id
	 */
	public long getAccount_id() {
		return account_id;
	}
	/**
	 * @param account_id the account_id to set
	 */
	public void setAccount_id(long account_id) {
		this.account_id = account_id;
	}
	/**
	 * @return the user_id
	 */
	public long getUser_id() {
		return user_id;
	}
	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	@Override
	public String toString() {
		return "-----------------------------------------------\n"
				 + "Accounts"
				 + "\nAccount ID: " + account_id
				 + "\nUser ID: " + user_id
				 + "\nBalance: " + balance
				 + "\n-----------------------------------------------";
	}

}
