package com.techelevator.tenmo.services;

import java.util.ArrayList;
import java.util.List;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


import com.techelevator.tenmo.models.Account;
import com.techelevator.tenmo.models.AuthenticatedUser;
import com.techelevator.tenmo.models.Transfers;

/*******************************************************************************************************
 * This is where you code Application Services required by your solution
 * 
 * Remember:  theApp ==> ApplicationServices  ==>  Controller  ==>  DAO
********************************************************************************************************/

public class TenmoApplicationServices {
	
	private final RestTemplate restTemplate = new RestTemplate();
	
	private static final String API_BASE_URL = "http://localhost:8080/";

	
//	public double getAccountBalance(String authToken) {
//		double accountBalance;
//		HttpEntity entity = new HttpEntity<>(makeAuthEntity(authToken));
//		accountBalance = restTemplate.exchange(API_BASE_URL +"accounts/balance", HttpMethod.GET, entity, double.class);
//		
//		return accountBalance;
	
	public Account[] listAccounts(AuthenticatedUser currentUser) {
		
    	HttpHeaders theHeaders = new HttpHeaders();
    	theHeaders.setBearerAuth(currentUser.getToken());
   	  	
    	theHeaders.setContentType(MediaType.APPLICATION_JSON);
   	  	
    	HttpEntity<Account[]> anEntity = new HttpEntity(theHeaders);
    	
	    ResponseEntity<Account[]> aResponse =  restTemplate.exchange(API_BASE_URL + "accounts", HttpMethod.GET, anEntity, Account[].class);
	    
	    return aResponse.getBody();
 
	}
	
	
	public Account getAccountBalanceByUserID(int id, AuthenticatedUser currentUser) {
		
    	HttpHeaders theHeaders = new HttpHeaders();
    	theHeaders.setBearerAuth(currentUser.getToken());
   	  	
    	theHeaders.setContentType(MediaType.APPLICATION_JSON);
   	  	
    	HttpEntity<Account> anEntity = new HttpEntity(theHeaders);
    	
	    ResponseEntity<Account> aResponse =  restTemplate.exchange(API_BASE_URL + "accounts/" + id + "/balance", HttpMethod.GET, anEntity, Account.class);
	    
	    return aResponse.getBody();
	     
	     
	    }
	
	public Transfers[] listOfTransfersByUserId (int id, AuthenticatedUser currentUser) {
		
    	HttpHeaders theHeaders = new HttpHeaders();
    	theHeaders.setBearerAuth(currentUser.getToken());
   	  	
    	theHeaders.setContentType(MediaType.APPLICATION_JSON);
   	  	
    	HttpEntity<Transfers[]> anEntity = new HttpEntity(theHeaders);
    	
	    ResponseEntity<Transfers[]> aResponse =  restTemplate.exchange(API_BASE_URL + "accounts/" + id + "/transfers", HttpMethod.GET, anEntity, Transfers[].class);
	    
	    return aResponse.getBody();
	
	    }
	
	public Transfers getTransferByTransferID(int id, AuthenticatedUser currentUser) {
		
    	HttpHeaders theHeaders = new HttpHeaders();
    	theHeaders.setBearerAuth(currentUser.getToken());
   	  	
    	theHeaders.setContentType(MediaType.APPLICATION_JSON);
   	  	
    	HttpEntity<Transfers> anEntity = new HttpEntity(theHeaders);
    	
	    ResponseEntity<Transfers> aResponse =  restTemplate.exchange(API_BASE_URL + "transfers/" + id, HttpMethod.GET, anEntity, Transfers.class);
	    
	    return aResponse.getBody();
	     
	     
	    }
	
	 public Transfers doTransfer(int fromAccount, int toAccount, double amount, AuthenticatedUser currentUser) {
		 
		 Transfers aTransfer = new Transfers();
		 
		 aTransfer.setTransfer_type_id(2);											// (2) Send transfer type
		 aTransfer.setAccount_to (toAccount);
		 aTransfer.setAccount_from(fromAccount);
		 aTransfer.setAmount(amount);
	        
	    	HttpHeaders theHeaders = new HttpHeaders();
	    	theHeaders.setBearerAuth(currentUser.getToken());
	   	  	
	    	theHeaders.setContentType(MediaType.APPLICATION_JSON);
	   	  	
	    	HttpEntity anEntity = new HttpEntity(aTransfer, theHeaders);
	   	  	
	    	aTransfer = restTemplate.postForObject( API_BASE_URL + "/accounts/transfers", anEntity, Transfers.class);
	   	  	
	    	return aTransfer;
	  }

	
	public List<Transfers> viewTransferHistory(String authToken) {
		List<Transfers> listOfTransfers = new ArrayList();
		
		return listOfTransfers;
	}
	
	public List<Account> getAccountList() {
		
		return null;
	}
	
	private HttpHeaders makeAuthEntity(String authToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.setBearerAuth(authToken);
		HttpEntity entity = new HttpEntity<>(headers);
		return headers;
		
	}
	
	


}

